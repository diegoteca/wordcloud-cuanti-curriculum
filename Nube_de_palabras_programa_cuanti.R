# Wordcloud for cuanti curriculum#

# Install
install.packages("tm")  # for text mining
install.packages("SnowballC") # for text stemming
install.packages("wordcloud") # word-cloud generator 
install.packages("RColorBrewer") # color palettes
install.packages("NLP") # Natural language packages
install.packages("pdftools") #For convert PDF to text
install.packages("readtext") # Read multiple types of text file
install.packages("utf8") # For posiibles problems with sime encoding files

# Load
library("NLP")
library("tm")
library("SnowballC")
library("RColorBrewer")
library("wordcloud")
library("pdftools")
library("readtext")
library("utf8")

sessionInfo()

Sys.getlocale() # Question for locale encoding
Sys.setlocale("LC_CTYPE", "C") # Set locale encoding

#Se va a buscar el archivo fuente, Trabajar directamente desde un pdf para análsis de 
#texto es algo complicado por la presencia de metadatos. Se necesita convertirlo a texto plano.

#Not only plain text. Is important that in this format avoid presence of metadata.
#For example, this is not possible in markdown, because this is plan text but with metadata

# For this task I use plain text. I copy the original file (google docs) and then paste into notepadd++.
# 

#I used "readtext" package that in turn use as input "pdftools" package, that in turn 
#use as input the poppler library#

programa_txt<- "https://raw.githubusercontent.com/diegoteca/worldcloud/master/Programa.txt"
programa_txt1<- readLines(programa_txt)

as_utf8(programa_txt1, normalize = TRUE)
utf8_valid(programa_txt1)

#Remove punctuation

programa_txt1 <- removePunctuation(programa_txt1,
                               preserve_intra_word_contractions = FALSE,
                               preserve_intra_word_dashes = FALSE,
                               ucp = TRUE)

#Characters as \r or \n are special characters for some functions.#

#Table of contents of pdf file"
###toc<- pdf_toc("38.pdf")

#Lee el texto (txt) como un corpus y le indico que lo tome como UTF-8

corpus_tesis_total_txt <- Corpus(VectorSource(tesis_txt))

#corpus_tesis_total_txt <- strsplit(corpus_tesis_total_txt, "\\r""\\n")


#We inspect the corpus#

inspect(corpus_tesis_total_txt)

#We edit the corpus for start to work with him. Se pasa a espacio los siguientes caracteres  / @ |. The first line
#is the funcion and teh other is the aplications of this function.

toSpace <- content_transformer(function (x , pattern ) gsub(pattern, " ", x))
corpus_tesis_total_txt <- tm_map(corpus_tesis_total_txt, toSpace, "/")
corpus_tesis_total_txt <- tm_map(corpus_tesis_total_txt, toSpace, "@")
corpus_tesis_total_txt <- tm_map(corpus_tesis_total_txt, toSpace, "\\|")

# Convert the text to lower case
corpus_tesis_total_txt <- tm_map(corpus_tesis_total_txt, content_transformer(tolower))
# Remove numbers
corpus_tesis_total_txt <- tm_map(corpus_tesis_total_txt, removeNumbers)
# Remove spanish common stopwords
corpus_tesis_total_txt <- tm_map(corpus_tesis_total_txt, removeWords, stopwords("spanish"))
# Remove your own stop word
corpus_tesis_total_txt <- tm_map(corpus_tesis_total_txt, removeWords, 
                                 c("muy", 
                                   "que", 
                                   "los",
                                   "las",
                                   "una",
                                   "del",
                                   "para",
                                   "como",
                                   "por",
                                   "con",
                                   "ser",
                                   "cada",
                                   "pero",
                                   "pero",
                                   "the",
                                   "and",
                                   "capítulo",
                                   "puede",
                                   "este",
                                   "esta",
                                   "entre",
                                   "diferent",
                                   "son",
                                   "esto",
                                   "sus",
                                   "tambien",
                                   "pueden",
                                   "mismo",
                                   "menos",
                                   "más",
                                   "-"))

#Eliminate extra white spaces
corpus_tesis_total_txt <- tm_map(corpus_tesis_total_txt, stripWhitespace)


#change some character with errors#

corpus_tesis_total_txt<- tm_map(corpus_tesis_total_txt, content_transformer(gsub), pattern = "\\b(C-)\\b", replacement = "i", fixed=TRUE)

#Text stemming
corpus_tesis_total_txt <- tm_map(corpus_tesis_total_txt, stemDocument, "spanish")



#Before term matrix we make a subset of corpus, exclude de bibliography#

corpus_tesis_txt <- corpus_tesis_total_txt[1:371]

#Se hace una matrix de términos (term document matrix or TDM)

tdm_total <- TermDocumentMatrix(corpus_tesis_txt, control = list(removePunctuation = TRUE,
                                                                 stopwords = TRUE))
m_total <- as.matrix(tdm_total)
v_total <- sort(rowSums(m_total),decreasing=TRUE)
lista_de_palabras_total <- data.frame(words = names(v_total),freq=v_total)
head(lista_de_palabras_total, 50)


#Se hace una nueva matrix de términos (term document matrix or TDM)

tdm_total <- TermDocumentMatrix(corpus_tesis__txt)
m_total <- as.matrix(tdm_total)
v_total <- sort(rowSums(m_total),decreasing=TRUE)
lista_de_palabras_total <- data.frame(words = names(v_total),freq=v_total)
head(lista_de_palabras_total, 50)

#Se hace nube de palabras

set.seed(1234)
wordcloud(words = lista_de_palabras_total$word, freq = lista_de_palabras_total$freq, min.freq = 5,
          max.words=300, random.order=FALSE, rot.per=0.35,
          colors=brewer.pal(8, "Dark2"))